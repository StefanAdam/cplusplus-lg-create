cvoiLG Create

Model - Class Definitions

Classes

createSite

 - Private Methods
 	- overload void .prepareWrite - prepares inputs to be added to file in .yml format
 	- void .writeVVV - opens VVVfile, writes prepared inputs to file, closes VVVfile
 	- string preparedInputs[array]

 - Public Methods
 	- void .fullCreate - Invoked to start a Full-Create process
 		- Inputs (host, url, user, pass, email, title, db-prefix)
 
 	- void .quickCreate - Invoked to start a Quick-Create process
 		- Inputs (url)
 		- 

 	- void .advancedCreate - Invoked to start a Advanced-Create process
 		- Inputs (repo, host, url, user, pass, email, title, db-prefix, plugins[array],themes[array],wp)


provisionSite

 - Private Methods
 	- 

 - Public Methods
 	- void .provisionStandalone - Invoked to start provision process
 		- Inputs (host)

 	- void .provisionAfterCreate - Argumentless, invoked automatically after a CreateSite public method completes.


View - Visual Components Only

- Define View
- Initiate .SiteCreate & .Provision Public methods

 - Start Screen Form
 	- Options - Get user input and provide as argument to getPath()
 	- Initiate Quick Create Form, Full Create Form, Advanced Create Form, Provision Form
 	- Initate instances of createSite and provisionSite (cS & pS)

 	- Quick Create Form
 		- Get user input and provide as argument to cS.quickCreate()

 	- Full Create Form
 		- Get user input and provide as argument to cS.fullCreate()

 	- Advanced Create Form
 		- Get user input and provide as argument to cS.advancedCreate()
 	- Provision Form
 		- Get user input and provide as argument to pS.provisionStandalone()



Controller - Business Logic

void checkConfig - Checks QT Settings for vvvPath, prompts to be set if unset.


 