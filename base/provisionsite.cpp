#include "provisionsite.h"


provisionSite::provisionSite()
{

}

QString provisionSite::MStoMinttyPath(QString path) {

    if(path.contains("/")) {
        qDebug() << "path had / (" << path << ")";
        QStringList pathBits = path.split("/");
        QStringList pathBitsClean;
        for (int i = 0; i < pathBits.count() -1; i++) {
            QString pathBit = pathBits[i];

            pathBit.replace(QString(":"), QString(""));
            pathBit.replace(QString("C"), QString("c"));

            pathBitsClean << pathBit;
        }
        QString pathClean = pathBitsClean.join("/");
        QString pathCleanFinal = "/" + pathClean;
        qDebug() << pathCleanFinal;
        return pathCleanFinal;
    }

    if(path.contains(":\\")) {
        qDebug() << "path had :\\ (" << path << ")";
        QStringList pathBits = path.split("\\");
        QStringList pathBitsClean;
        //pathBitsClean << "/";

        for (int i = 0; i < pathBits.count() -1; i++) {

            QString pathBit = pathBits[i];

            pathBit.replace(QString(":"), QString(""));
            pathBit.replace(QString("C"), QString("c"));
            //qDebug() << pathBit;
            pathBitsClean << pathBit;
        }
        QString pathClean = pathBitsClean.join("/");
        QString pathCleanFinal = "/" + pathClean;
        qDebug() << pathCleanFinal;
        return pathCleanFinal;
    }
    qDebug() << "Path was non-recognized Format";
    qFatal("Path Formatting Error");

}

void provisionSite::alterVVV(QString key) {

    QSettings settings;
    QString vvvPath = settings.value("vvvPath").toString();
    QFile vvvFile(vvvPath);
    QTemporaryFile temp;
    key = "    " + key + ":";

    if(!vvvFile.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Could not open file for reading";
        throw std::logic_error("Could not open file for Reading");
    }

    QTextStream vvvStream(&vvvFile);
    bool currentSite =  false;
    while (!vvvStream.atEnd())
    {
        QString line = vvvStream.readLine();
        if(line == key)
        {
            currentSite = true;
        }

        if(line == "        skip_provisioning: false" && currentSite == false ) {
            line = "        skip_provisioning: true";
        }

        if(line == "        skip_provisioning: true" && currentSite == true) {
            line = "        skip_provisioning: false";
            currentSite = false;
        }

        if(!temp.open()) {
            qDebug() << "Could not open temp file";
            throw std::logic_error("Could not open temp file");
        }
        QTextStream tempStream(&temp);
        //qDebug() << line;
        line = line + "\n";
        tempStream << line;
        tempStream.flush();

    }
    temp.seek(0);

    vvvFile.close();

    QString vvvContents = temp.readAll();
    qDebug() << "vvvContents: " << vvvContents;
    if(!vvvFile.open(QFile::WriteOnly | QFile::Truncate))
    {
        qDebug() << "Could not open file for reading";
        throw std::logic_error("Could not open file for Reading");
    }

    vvvStream << vvvContents;
    vvvFile.close();
}

void provisionSite::createShellScript(QString key) {
    QString result;
    QSettings settings("AlteroPax Industries", "LG-Create");
    QString vvvPath = settings.value("vvvPath").toString();
    qDebug() << "path from settings: " << vvvPath;
    qDebug() << "path from MStoMinttyPath: " << provisionSite::MStoMinttyPath(vvvPath);
    QFile file("provision.sh");
    if(file.exists()) {
        file.remove();
    }
   if( file.open(QIODevice::WriteOnly ) ) {
        QTextStream pStream(&file);

        pStream << "cd "+ provisionSite::MStoMinttyPath(vvvPath)+";\n";
        pStream << "echo 'LOGGER -> STARTING VAGRANT';\n" ;
        pStream << "vagrant up;\n";
        pStream << "echo 'LOGGER -> CHECKING VAGRANT STATUS';\n" ;
        pStream << "vagrant status;\n";
        pStream << "echo 'LOGGER -> STARTING PROVISION';\n" ;
        pStream << "vagrant provision;\n";
    }

   return;
}

void provisionSite::provision(QString key) {

    provisionSite::alterVVV(key);

    provisionSite::createShellScript(key);


    #if (defined (Q_OS_MAC))
        QSettings settings;
        if ( settings.value("debug") == 1) {
            qDebug() << system("pwd");
        }
        system("chmod +x ./provision.sh");
        system("./provision.sh");
    #endif

    #if (defined (Q_OS_WIN))

        system("provision.sh");

    #endif

}

void provisionSite::provisionAfterCreate() {

}
