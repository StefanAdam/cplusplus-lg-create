#include <QCoreApplication>
#include <QSettings>
#include <QString>
#include <QDebug>
#include <iostream>
#include <string>
#include "helpers.h"
#include  "createsite.h"
#include "provisionsite.h"



int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);
    a.quit();
    QSettings* settings = new QSettings("AlteroPax Industries", "LG-Create");

    checkConfig();

        for (int i = 0; i < a.arguments().count(); i++ ){

           if(a.arguments().at(i) == "--debug=0") {
               settings->setValue("debug", 0);
            }
           if(a.arguments().at(i) == "--debug=1") {
               settings->setValue("debug", 1);
           }
           if(a.arguments().at(i) == "--showarguments") {
               for  (int x =0; x < a.arguments().count(); ++x ) {
                    QString debugArgue = a.arguments().at(x);
                    std::string stdDebugArgue = debugArgue.toStdString();
                    std::cout << stdDebugArgue << std::endl;
               }
           }

           if(a.arguments().at(i) == "--whatdebug") {
               QString Qdebuglevel = settings->value("debug").toString();
               std::string debuglevel = Qdebuglevel.toStdString();
               std::cout << debuglevel << "\n";
           }

           if(a.arguments().at(i) == "--changepath"){
                QString path = getPath();
                setPath(path);
           }

            if(a.arguments().at(i) == "--showpath"){
                showPath();
            }

            if(a.arguments().at(i) == "--clearpath"){
                clearPath(settings);
            }

            if(a.arguments().at(i) == "--quickcreate"){
              createSite cS; // Instantiate a Quick Create Object
              std::string url;
              // Get Inputs
              std::cout << "Site URL: ";
              std::cin >> url;
              // Convert Inputs
              QString qURL = QString::fromStdString(url);
              // Check Validity
              cS.checkVVV();
              // Run Quick Create
              cS.quickCreate(qURL);

              // Debug Skip Provision
              if (settings->value("debug") == 1) {
                  std::string response;
                  std::cout << "Provision? (y/n):  ";
                  std::cin >> response;
                  if (response != "y") {
                  goto skip;
                  }
              }

              // Convert qURL to key and run provisioner
              QString key = urlToKey(qURL);
              provisionSite pS;
              pS.provision(key);

            }

            if(a.arguments().at(i) == "--multicreate"){
              createSite cS; // Instantiate a Quick Create Object
              std::string url;
              // Get Inputs
              QStringList qURLs;
              while (true) {
                  std::cout << "Site URL: ";
                  std::cin >> url;
                  QString qURL = QString::fromStdString(url);
                  qURLs << qURL;
                  std::cout << "Another URL? (y/n): ";
                  std::string response;
                  std::cin >> response;
                  if (response == "n") {
                      break;
                  }
              }

              // Convert Inputs

              // Check Validity
              cS.checkVVV();
              // Run Quick Create
              cS.multisiteCreate(qURLs);

              // Debug Skip Provision
              if (settings->value("debug") >= 1) {
                  std::string response;
                  std::cout << "Provision? (y/n):  ";
                  std::cin >> response;
                  if (response != "y") {
                  goto skip;
                  }
              }

              // Convert qURL to key and run provisioner
              QString key = urlToKey(qURLs[0]);
              provisionSite pS;
              pS.provision(key);

            }
skip:
            if(a.arguments().at(i) == "--fullcreate"){

              createSite cS; // Instantiate a createSite Object

              // Define all variables required
              std::string url;
              std::string user;
              std::string pass;
              std::string email;
              std::string title;
              std::string dbprefix;
              std::string multisite;
              std::string xipio;
              std::string delplugins;
              std::string delthemes;
              std::string wp;

              // Get all variables required
              std::cout << "Site URL: ";
              std::cin >> url;
              std::cout << "Site User: ";
              std::cin >> user;
              std::cout << "Site Password: ";
              std::cin >> pass;
              std::cout << "Site Email: ";
              std::cin >> email;
              std::cout << "Site Title: ";
              std::cin >> title;
              std::cout << "Site DBPrefix: ";
              std::cin >> dbprefix;
              std::cout << "Multisite: (true or false) ";
              std::cin >> multisite;
              std::cout << "XipIO: (true or false) ";
              std::cin >> xipio;
              std::cout << "Delete Default Plugins: (true or false) ";
              std::cin >> delplugins;
              std::cout << "Delete Default Themes: (true or false) ";
              std::cin >> delthemes;
              std::cout << "Configure Wordpress?: (true or false) ";
              std::cin >> wp;

              // Convert inputs from std::string to QString for processing
              QString qURL = QString::fromStdString(url);
              QString qUser = QString::fromStdString(user);
              QString qPass = QString::fromStdString(pass);
              QString qEmail = QString::fromStdString(email);
              QString qTitle = QString::fromStdString(title);
              QString qDBPrefix = QString::fromStdString(dbprefix);
              QString qMultisite = QString::fromStdString(multisite);
              QString qXipio = QString::fromStdString(xipio);
              QString qDelPlugins = QString::fromStdString(delplugins);
              QString qDelThemes = QString::fromStdString(delthemes);
              QString qWP = QString::fromStdString(wp);

              // Run Full Create with converted inputs
              qDebug() << "Arguments passed to Full Create" << qURL <<  qUser << qPass << qEmail << qTitle << qDBPrefix << qMultisite << qXipio << qDelPlugins << qDelThemes << qWP;
              cS.checkVVV();
              cS.fullCreateLegacy(qURL, qUser, qPass, qEmail, qTitle, qDBPrefix, qMultisite, qXipio, qDelPlugins, qDelThemes, qWP);

              // Run Provisioner with cleaned URL
              QString key = urlToKey(qURL);
              provisionSite pS;
              pS.provision(key);

            }

            if(a.arguments().at(i) == "--advancedcreate"){
              throw std::logic_error("Not Implemented"); // TODO Develop support for more advanced object processing
              createSite cS; // Instantiate a createSite Object

              // Define all variables required
              std::string url;
              std::string user;
              std::string pass;
              std::string email;
              std::string title;
              std::string dbprefix;
              std::string multisite;
              std::string xipio;
              std::string delplugins;
              std::string delthemes;
              std::string wp;
              std::string plugin;
              std::string theme;
              QStringList qlPlugin;
              QStringList qlTheme;

              char yn;

              // Get all variables required
              std::cout << "Site URL: ";
              std::cin >> url;
              std::cout << "Site User: ";
              std::cin >> user;
              std::cout << "Site Password: ";
              std::cin >> pass;
              std::cout << "Site Email: ";
              std::cin >> email;
              std::cout << "Site Title: ";
              std::cin >> title;
              std::cout << "Site DBPrefix: ";
              std::cin >> dbprefix;
              std::cout << "Multisite: (true or false) ";
              std::cin >> multisite;
              std::cout << "XipIO: (true or false) ";
              std::cin >> xipio;
              std::cout << "Delete Default Plugins: (true or false) ";
              std::cin >> delplugins;
              std::cout << "Delete Default Themes: (true or false) ";
              std::cin >> delthemes;
              std::cout << "Configure Wordpress?: (true or false) ";
              std::cin >> wp;

              // Assemble Plugin URL Array
              while (bool cont = true) {
                 std::cout << "Install Additional Plugins?: (y/n) ";
                 std::cin >> yn;

                 if (toupper(yn) == 'Y') {
                      std::cout << "Enter Plugin URL or Slug\n";
                      std::cin >> plugin;
                      QString qPlugin;
                      qPlugin = QString::fromStdString(plugin);
                      qDebug() << qPlugin;
                      qlPlugin << qPlugin;
                      plugin = "\0";
                      qPlugin = "\0";
                 }
                 else {
                     cont = false;
                     break;
                 }
              }


              yn = '\0';

              //Assemble Theme URL Array
              while (bool cont = true) {
                std::cout << "Install Additional Themes?: (y/n ";
                std::cin >> yn;
                if (toupper(yn) =='Y') {
                    std::cout << "Enter Theme URL or Slug\n";
                    std::cin >> theme;
                    QString qTheme;
                    qTheme = QString::fromStdString(theme);
                    qDebug() << qTheme;
                    qlTheme << qTheme;
                }
                else {
                    cont = false;
                    break;
                }
              }

              // Convert inputs from std::string to QString for processing
              QString qURL = QString::fromStdString(url);
              QString qUser = QString::fromStdString(user);
              QString qPass = QString::fromStdString(pass);
              QString qEmail = QString::fromStdString(email);
              QString qTitle = QString::fromStdString(title);
              QString qDBPrefix = QString::fromStdString(dbprefix);
              QString qMultisite = QString::fromStdString(multisite);
              QString qXipio = QString::fromStdString(xipio);
              QString qDelPlugins = QString::fromStdString(delplugins);
              QString qDelThemes = QString::fromStdString(delthemes);
              QString qWP = QString::fromStdString(wp);

             /* qDebug() << "Simple Arguments passed to advancedCreate" << qURL <<  qUser << qPass << qEmail << qTitle << qDBPrefix << qMultisite << qXipio << qDelPlugins << qDelThemes << qWP;
              qDebug() << "Complex Arguments Passed to advancedCreated";
              for (int x = 0; x < qlPlugin.count(); x++) {
                  qDebug() << "qlPlugin element["<<x<<"]" << qlPlugin[x];
              }
              for (int y = 0; y < qlTheme.count(); y++) {
                  qDebug() << "qlTheme element["<<y<<"]" << qlTheme[y];
              } */


              cS.checkVVV();
              cS.advancedCreate(qURL, qlPlugin, qlTheme, qUser, qPass, qEmail, qTitle, qDBPrefix, qMultisite, qXipio, qDelPlugins, qDelThemes, qWP);

            }

            if(a.arguments().at(i) == "--provisionstandalone"){

                std::string url;

                std::cout << "Enter VVVCustom.yml Url to provision: ";
                std::cin >> url;
                QString qURL = QString::fromStdString(url);

                QString key = urlToKey(qURL);

                provisionSite pS;
                pS.provision(key);
            }
    }





    return 0;
}


