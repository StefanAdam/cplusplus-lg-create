#ifndef PROVISIONSITE_H
#define PROVISIONSITE_H
#include <QString>
#include <QProcess>
#include <QSettings>
#include <QDebug>
#include <QDir>
#include <QFile>
#include <QFileInfo>
#include <QTemporaryFile>
#include <iostream>

class provisionSite
{
private:
    void createShellScript(QString);
    void alterVVV(QString);
    QString MStoMinttyPath(QString);

public:
    provisionSite();
    void provision();
    void provision(QString);
    void provisionAfterCreate();
};

#endif // PROVISIONSITE_H
