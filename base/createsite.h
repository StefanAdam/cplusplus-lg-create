#ifndef CREATESITE_H
#define CREATESITE_H
#include <QCoreApplication>
#include <QSettings>
#include <QString>
#include <QTextStream>
#include <QFile>
#include <QDebug>



class createSite
{
private:
    void prepareInputLegacy(QString url, QString user, QString pass, QString email, QString title, QString dbprefix, QString multisite, QString xipio, QString DelPlugins, QString DelThemes, QString wp);
    void prepareInput(QString url, QString user, QString pass, QString email, QString title, QString dbprefix, QString wp_type);
    void prepareInputLegacy(QString url, QStringList plugins, QStringList themes, QString user, QString pass, QString email, QString title, QString dbprefix, QString multisite, QString xipio, QString DelPlugins, QString DelThemes, QString wp);
    void prepareInputLegacy(QStringList url, QString user, QString pass, QString email, QString title, QString dbprefix, QString multisite, QString xipio, QString DelPlugins, QString DelThemes, QString wp);
    void prepareInput(QStringList url, QString user, QString pass, QString email, QString title, QString dbprefix, QString site_type);
    void writeVVV();

    QString ymlEscapeURL(QString URL);

    QStringList preparedInputs;
    QString space = " ";
    QString tab4 = "    ";
    QString tab8 = tab4 + tab4;
    QString tab12 = tab8 + tab4;
    QString tab16 = tab8 + tab8;
    QString dash = "- ";
    QString colon =":";
    QString newline = "\n";

public:
    void quickCreate(QString url);
    void quickCreateLegacy(QString url, QString wp);
    void fullCreate(QString url, QString user, QString pass, QString email, QString title, QString dbprefix, QString  site_type );
    void fullCreateLegacy(QString url, QString user, QString pass, QString email, QString title, QString dbprefix, QString multisite, QString xipio, QString delplugins, QString delthemes, QString wp);
    void advancedCreate(QString url, QStringList plugins, QStringList themes, QString user, QString pass, QString email, QString title, QString dbprefix, QString multisite, QString xipio, QString delplugins, QString delthemes, QString wp);
    void checkVVV();
    void multisiteCreate(QStringList url);
    void multisiteCreateLegacy(QStringList url);

};



#endif // CREATESITE_H
