#include "helpers.h"


QString getPath() {
    std::string path;
    std::cout << "Please enter the path to your VVV custom file: ";
    std::cin >> path;
    QString qPath = QString::fromStdString(path);
    return qPath;
}

QString getPath(QString qPath) {
    return qPath;
}

void setPath(QString vvvPath){
    QSettings settings("AlteroPax Industries", "LG-Create");
    settings.setValue("vvvPath", vvvPath);
}

void showPath() {
    QSettings settings("AlteroPax Industries", "LG-Create");
    QString vvvPath = settings.value("vvvPath").toString();
    std::string vvvPathStdout = vvvPath.toStdString();
    std::cout << vvvPathStdout << std::endl;
}

void clearPath(QSettings* settings) {
    settings->remove("vvvPath");
}

void checkConfig(){
    QSettings settings("AlteroPax Industries", "LG-Create");

    if(!settings.contains("vvvPath")){
        std::cout << "vvvPath is unset" << std::endl;
        QString vvvPath = getPath();
        setPath(vvvPath);
    }
}

QString urlToKey(QString url) {
    QStringList keyList = url.split('.');
    QString key = keyList[0];
    key.replace(QString("-"), QString(""));

    return key;
}
