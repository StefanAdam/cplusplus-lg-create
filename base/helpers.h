#ifndef HELPERS_H
#define HELPERS_H
#include <iostream>
#include <string>
#include <QString>
#include <QSettings>

QString getPath();
QString getPath(QString qPath);
void setPath(QString vvvPath);
void showPath();
void clearPath(QSettings* settings);
void checkConfig();
QString urlToKey(QString url);



#endif // HELPERS_H
