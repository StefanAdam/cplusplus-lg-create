#include "createsite.h"


QString createSite::ymlEscapeURL(QString url) {
    url.replace(QString("-"), QString(""));
    return  url;
}

Q_DECL_DEPRECATED void createSite::prepareInputLegacy(QString url, QString wp, QString user = "lg-admin", QString pass = "password", QString email = "admin@longevitygraphics.com", QString title = "LocalDev", QString dbprefix ="cms_", QString multisite = "false", QString xipio = "false", QString DelPlugins = "true", QString DelThemes = "true") {
    QSettings settings("AlteroPax Industries", "LG-Create");
    QStringList urllist = url.split('.');
    QString vvvKey = urllist[0];
    qDebug() << "This Method is deprecated with VVV2, please ensure your site is in a VVV2 box";

    QString pKey = tab4 + ymlEscapeURL(vvvKey) + colon + newline;
    QString pRepo = tab8 + "repo" + colon + space + "https://github.com/JPry/vvv-base.git" + newline;
    QString pHost = tab8 + "hosts" + colon + newline;
    QString pURL = tab12 + dash + url + newline;
    QString pCustom = tab8 + "custom" + colon + newline;
    QString pUser = tab12 + "admin_user" + colon + space + user + newline;
    QString pPass = tab12 + "admin_password" + colon + space + pass + newline;
    QString pEmail = tab12 + "admin_email" + colon + space + email + newline;
    QString pTitle = tab12 + "title" + colon + space + title + newline;
    QString pDBPrefix = tab12 + "db_prefix" + colon + space + dbprefix + newline;
    QString pMultisite = tab12 + "multisite" + colon + space + multisite + newline;
    QString pXipIO = tab12 + "xipio" + colon + space + xipio + newline;
    QString pPHead = tab12 + "plugins" + colon + newline;
    QString pPlugins = tab16 + dash + "worker" + newline;
    QString pTHead = tab12 + "themes" + colon + newline;
    QString pThemes = tab16 + dash + "https://bitbucket.org/lgadmin/wp-theme-parent/get/HEAD.zip" + newline;
    QString pDelPlugins = tab12 + "delete_default_plugins" + colon + space + DelPlugins + newline;
    QString pDelThemes = tab12 + "delete_default_themes" + colon + space + DelThemes + newline;
    QString pWP = tab12 + "wp" + colon + space + wp + newline;

    this->preparedInputs << newline << pKey << pRepo << pHost << pURL << pCustom << pUser << pPass << pEmail << pTitle << pDBPrefix << pMultisite << pXipIO << pPHead << pPlugins << pTHead << pThemes << pDelPlugins << pDelThemes << pWP;

    if ( settings.value("debug") >= 1) {
        for (int x = 0; x < preparedInputs.count(); x++) {
            qDebug() << "PreparedInput["<< x << "] was: " << preparedInputs[x];
        }
    }



};

Q_DECL_DEPRECATED void createSite::prepareInputLegacy(QString url, QStringList plugins, QStringList themes, QString user = "lg-admin", QString pass = "password", QString email = "admin@longevitygraphics.com", QString title = "LocalDev", QString dbprefix ="cms_", QString multisite = "false", QString xipio = "false", QString DelPlugins = "true", QString DelThemes = "true", QString wp = "true") {
    qDebug() << "This Method is deprecated with VVV2, please ensure your site is in a VVV2 box";
    QStringList urllist = url.split('.');
    QString vvvKey = urllist[0];

    QString pKey = tab4 + ymlEscapeURL(vvvKey) + colon + newline;
    QString pRepo = tab8 + "repo" + colon + space + "https://github.com/JPry/vvv-base.git" + newline;
    QString pHost = tab8 + "hosts" + colon + newline;
    QString pURL = tab12 + dash + url + newline;
    QString pCustom = tab8 + "custom" + colon + newline;
    QString pUser = tab12 + "admin_user" + colon + space + user + newline;
    QString pPass = tab12 + "admin_password" + colon + space + pass + newline;
    QString pEmail = tab12 + "admin_email" + colon + space + email + newline;
    QString pTitle = tab12 + "title" + colon + space + title + newline;
    QString pDBPrefix = tab12 + "db_prefix" + colon + space + dbprefix + newline;
    QString pMultisite = tab12 + "multisite" + colon + space + multisite + newline;
    QString pXipIO = tab12 + "xipio" + colon + space + xipio + newline;
    QString pPHead = tab12 + "plugins" + colon + newline;
    QString pTHead = tab12 + "themes" + colon + newline;

    QString pDelPlugins = tab12 + "delete_default_plugins" + colon + space + DelPlugins + newline;
    QString pDelThemes = tab12 + "delete_default_themes" + colon + space + DelThemes + newline;
    QString pWP = tab12 + "wp" + colon + space + wp + newline;

    QStringList pPlugins;

    pPlugins[0] = tab16 + dash + "worker" + newline;

    for (int x = 0; x < plugins.count(); x++)
    {
        pPlugins << tab16 + dash + plugins[x] + newline;
    }

    QStringList pThemes;

    pThemes[0] = tab16 + dash + "https://bitbucket.org/lgadmin/wp-theme-parent/get/HEAD.zip" + newline;

    for (int x = 0; x < themes.count(); x++)
    {
        pThemes << tab16 + dash + themes[x] + newline;
    }

    this->preparedInputs << newline << pKey << pRepo << pHost << pURL << pCustom << pUser << pPass << pEmail << pTitle << pDBPrefix << pMultisite << pXipIO << pPHead;

    for (int x = 0; x < pPlugins.count(); x++)
    {
       this->preparedInputs << pPlugins[x];
    }

    this->preparedInputs << pTHead;

    for (int x = 0; x < pThemes.count(); x++)
    {
       this->preparedInputs << pThemes[x];
    }

    this->preparedInputs << pDelPlugins << pDelThemes << pWP;

    for (int x = 0; x < preparedInputs.count(); x++) {
        qDebug() << "PreparedInput["<< x << "] was: " << preparedInputs[x];
    }
};

Q_DECL_DEPRECATED void createSite::prepareInputLegacy(QStringList url, QString user = "lg-admin", QString pass = "password", QString email = "admin@longevitygraphics.com", QString title = "LocalDev", QString dbprefix ="cms_", QString multisite = "true", QString xipio = "false", QString DelPlugins = "true", QString DelThemes = "true", QString wp = "true") {
   QSettings settings("AlteroPax Industries", "LG-Create");
   QStringList urllist = url[0].split('.');
   QString vvvKey = urllist[0];
   QStringList pURLList;

   qDebug() << "This Method is deprecated with VVV2, please ensure your site is in a VVV2 box";

   QString pKey = tab4 + ymlEscapeURL(vvvKey) + colon + newline;
   QString pRepo = tab8 + "repo" + colon + space + "https://github.com/JPry/vvv-base.git" + newline;
   QString pHost = tab8 + "hosts" + colon + newline;
   for (int x = 0; x < url.count(); ++x) {
    QString pURL = tab12 + dash + url[x] + newline;
    pURLList << pURL;
   }
   QString pCustom = tab8 + "custom" + colon + newline;
   QString pUser = tab12 + "admin_user" + colon + space + user + newline;
   QString pPass = tab12 + "admin_password" + colon + space + pass + newline;
   QString pEmail = tab12 + "admin_email" + colon + space + email + newline;
   QString pTitle = tab12 + "title" + colon + space + title + newline;
   QString pDBPrefix = tab12 + "db_prefix" + colon + space + dbprefix + newline;
   QString pMultisite = tab12 + "multisite" + colon + space + multisite + newline;
   QString pXipIO = tab12 + "xipio" + colon + space + xipio + newline;
   QString pPHead = tab12 + "plugins" + colon + newline;
   QString pPlugins = tab16 + dash + "worker" + newline;
   QString pTHead = tab12 + "themes" + colon + newline;
   QString pThemes = tab16 + dash + "https://bitbucket.org/lgadmin/wp-theme-parent/get/HEAD.zip" + newline;
   QString pDelPlugins = tab12 + "delete_default_plugins" + colon + space + DelPlugins + newline;
   QString pDelThemes = tab12 + "delete_default_themes" + colon + space + DelThemes + newline;
   QString pWP = tab12 + "wp" + colon + space + wp + newline;

   this->preparedInputs << newline << pKey << pRepo << pHost;
   for (int x = 0; x < pURLList.count(); ++x ) {
       this->preparedInputs << pURLList[x];
   }
   this->preparedInputs << pCustom << pUser << pPass << pEmail << pTitle << pDBPrefix << pMultisite << pXipIO << pPHead << pPlugins << pTHead << pThemes << pDelPlugins << pDelThemes << pWP;

}

void createSite::prepareInput(QString url, QString user = "lg-admin", QString pass = "password", QString email = "admin@longevitygraphics.com", QString title = "LocalDev", QString dbprefix ="cms_", QString site_type = "single") {
    QSettings settings("AlteroPax Industries", "LG-Create");
    QStringList urllist = url.split('.');
    QString vvvKey = urllist[0];

    QString pKey = tab4 + ymlEscapeURL(vvvKey) + colon + newline;
    QString pRepo = tab8 + "repo" + colon + space + "https://bitbucket.org/longevitygraphics/lg-base.git" + newline;
    QString pDescription = tab8 + "description" + colon + space + "\"A LG Dev Site\"" + newline;
    QString pSkipProv = tab8 + "skip_provisioning" + colon + space + "false" + newline;
    QString pHost = tab8 + "hosts" + colon + newline;
    QString pURL = tab12 + dash + url + newline;
    QString pCustom = tab8 + "custom" + colon + newline;
    QString pType = tab12 + "wp_type" + colon + space + site_type + newline;
    QString pUser = tab12 + "admin_user" + colon + space + user + newline;
    QString pPass = tab12 + "admin_password" + colon + space + pass + newline;
    QString pEmail = tab12 + "admin_email" + colon + space + email + newline;
    QString pTitle = tab12 + "title" + colon + space + title + newline;
    QString pDBPrefix = tab12 + "db_prefix" + colon + space + dbprefix + newline;
    QString pPHead = tab12 + "plugins" + colon + newline;
    QString pPlugins = tab16 + dash + "worker" + newline;
    QString pTHead = tab12 + "themes" + colon + newline;
    QString pThemes = tab16 + dash + "https://bitbucket.org/lgadmin/wp-theme-parent/get/HEAD.zip" + newline;

    this->preparedInputs << newline << pKey << pRepo << pDescription << pSkipProv << pHost << pURL << pCustom << pType << pUser << pPass << pEmail << pTitle << pDBPrefix << pPHead << pPlugins << pTHead << pThemes;

    if ( settings.value("debug") >= 1) {
        for (int x = 0; x < preparedInputs.count(); x++) {
            qDebug() << "PreparedInput["<< x << "] was: " << preparedInputs[x];
        }
    }
};

void createSite::prepareInput(QStringList url, QString user = "lg-admin", QString pass = "password", QString email = "admin@longevitygraphics.com", QString title = "LocalDev", QString dbprefix ="cms_", QString site_type = "subdirectory") {
   QSettings settings("AlteroPax Industries", "LG-Create");
   QStringList urllist = url[0].split('.');
   QString vvvKey = urllist[0];
   QStringList pURLList;

   QString pKey = tab4 + ymlEscapeURL(vvvKey) + colon + newline;
   QString pRepo = tab8 + "repo" + colon + space + "https://bitbucket.org/longevitygraphics/lg-base.git" + newline;
   QString pSkipProv = tab8 + "skip_provisioning" + colon + space + "false" + newline;
   QString pDescription = tab8 + "description" + colon + space + "\"A LG Dev Site\"" + newline;
   QString pHost = tab8 + "hosts" + colon + newline;
   for (int x = 0; x < url.count(); ++x) {
    QString pURL = tab12 + dash + url[x] + newline;
    pURLList << pURL;
   }
   QString pCustom = tab8 + "custom" + colon + newline;
   QString pType = tab12 + "wp_type" + colon + space + site_type + newline;
   QString pUser = tab12 + "admin_user" + colon + space + user + newline;
   QString pPass = tab12 + "admin_password" + colon + space + pass + newline;
   QString pEmail = tab12 + "admin_email" + colon + space + email + newline;
   QString pTitle = tab12 + "title" + colon + space + title + newline;
   QString pDBPrefix = tab12 + "db_prefix" + colon + space + dbprefix + newline;
   QString pPHead = tab12 + "plugins" + colon + newline;
   QString pPlugins = tab16 + dash + "worker" + newline;
   QString pTHead = tab12 + "themes" + colon + newline;
   QString pThemes = tab16 + dash + "https://bitbucket.org/lgadmin/wp-theme-parent/get/HEAD.zip" + newline;

   this->preparedInputs << newline << pKey << pRepo << pDescription << pSkipProv << pHost;
   for (int x = 0; x < pURLList.count(); ++x ) {
       this->preparedInputs << pURLList[x];
   }
   this->preparedInputs << pCustom << pType << pUser << pPass << pEmail << pTitle << pDBPrefix << pPHead << pPlugins << pTHead << pThemes;

}

void createSite::checkVVV() {

    QSettings settings("AlteroPax Industries", "LG-Create");
    QString vvvPath = settings.value("vvvPath").toString();
    QFile vvvFile(vvvPath);

    if(!vvvFile.open(QFile::ReadOnly | QFile::Text))
    {
        qDebug() << "Could not open file for reading";
        throw std::logic_error("Could not open file for Reading");
    }

    QTextStream vvvStream(&vvvFile);

    bool vStartfound = false;

    while (!vvvStream.atEnd() && vStartfound != true)
    {
        QString line = vvvStream.readLine();
        if (line == "sites:") {
            vStartfound = true;
            break;
        }
    }
    if (vStartfound == true) {
        return;
    }
    else {
        vvvFile.close();
        if (vvvFile.open(QIODevice::WriteOnly | QIODevice::Append))
        {
            // Stream text to file
            QTextStream vvvStream(&vvvFile);

            vvvStream << newline + "sites:";
            return;
        }
        throw std::logic_error("Failed to find Valid VVV start point");
    }

}

void createSite::writeVVV(){
    QSettings settings("AlteroPax Industries", "LG-Create");
    QString vvvPath = settings.value("vvvPath").toString();
    QFile vvvFile(vvvPath);

    if (vvvFile.open(QIODevice::WriteOnly | QIODevice::Append))
    {
        // Stream text to file
        QTextStream vvvStream(&vvvFile);

        for (int x = 0; x < preparedInputs.count(); x++) {
            if (settings.value("debug") == 1) {
                qDebug() << "prepared input [" << x<< "] was: " << preparedInputs[x];
            }
           vvvStream << this->preparedInputs[x];


        }
        vvvFile.close();
    }
    else {
        throw std::logic_error("Failed to Open File for writing");
    }
    return;
};

void createSite::quickCreate(QString url) {
    prepareInput(url);
    checkVVV();
    writeVVV();
};


Q_DECL_DEPRECATED void createSite::quickCreateLegacy(QString url, QString wp) {
    prepareInputLegacy(url, wp);
    checkVVV();
    writeVVV();
}

Q_DECL_DEPRECATED void createSite::fullCreateLegacy(QString url, QString user, QString pass, QString email, QString title, QString dbprefix, QString multisite, QString xipio, QString delplugins, QString delthemes, QString wp) {
    prepareInputLegacy(url, user, pass, email, title, dbprefix, multisite, xipio, delplugins, delthemes, wp);
    writeVVV();
    //provisionSite::provisionStandalone();
}

Q_DECL_DEPRECATED void createSite::advancedCreate(QString url, QStringList plugins, QStringList themes, QString user, QString pass, QString email, QString title, QString dbprefix, QString multisite, QString xipio, QString delplugins, QString delthemes, QString wp) {
    prepareInputLegacy(url, plugins, themes, user, pass, email, title, dbprefix, multisite, xipio, delplugins, delthemes, wp);
}

Q_DECL_DEPRECATED void createSite::multisiteCreateLegacy(QStringList url) {
    prepareInputLegacy(url);
    checkVVV();
    writeVVV();

}

void createSite::multisiteCreate(QStringList url) {
    prepareInput(url);
    checkVVV();
    writeVVV();

}

void createSite::fullCreate(QString url, QString user, QString pass, QString email, QString title, QString dbprefix, QString site_type) {
    prepareInput(url, user, pass, email, title, dbprefix, site_type);
}



