#ifndef FULLCREATEFORM_H
#define FULLCREATEFORM_H

#include <QDialog>
#include "common.h"
#include "../base/createsite.h"
#include "../base/provisionsite.h"

namespace Ui {
class fullCreateForm;
}

class fullCreateForm : public QDialog
{
    Q_OBJECT

public:
    explicit fullCreateForm(QWidget *parent = nullptr);
    ~fullCreateForm();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::fullCreateForm *ui;
};

#endif // FULLCREATEFORM_H
