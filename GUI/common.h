#ifndef COMMON_H
#define COMMON_H
#include <QString>
#include <QObject>
#include <QSettings>
#include <QDebug>
#include <QFileDialog>
#include <QDialog>
#include <QUrl>
#include <QVersionNumber>
#include <QDialogButtonBox>
#include "pathrequireddialog.h"
#include "network.h"

void checkSettings(QWidget*);
void setPath(QString);
QString getPath(QWidget*);
QString urlToKey(QString);
const static QVersionNumber version(1,3,1);
void updateCheck();


#endif // COMMON_H
