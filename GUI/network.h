#ifndef NETWORK_H
#define NETWORK_H

#include <QObject>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QSettings>
#include <QMessageBox>
#include <QProgressBar>
#include <QVBoxLayout>
#include <QVersionNumber>
//#include <common.h>
//#include <settingsform.h>


class network : public QObject
{
    Q_OBJECT
    QNetworkAccessManager manager;
    QNetworkReply* latestVersion;


public:
    network();
    void getLatestVersion();
    void updateLatest(const QUrl &url);

signals:
    void downloadCompleted();
    void downloadStarted();

private:
    QString saveFileName(const QUrl &url);
    bool saveToDisk(const QString &filename, QIODevice *data);
    QWidget* dlprogress;
    QProgressBar* dlprogressBar;
    QLabel* dlprogressText;

private slots:
    void process_version(QNetworkReply *reply);
    void downloadFinished(QNetworkReply *reply);
    void downloadProgress(qint64 ist, qint64 max);
    void downloadProgressBox();
    void downloadProgressCompleted();

};


#endif // NETWORK_H
