#include "quickcreateform.h"
#include "ui_quickcreateform.h"
#include "../base/createsite.cpp"
#include "../base/provisionsite.cpp"



quickCreateForm::quickCreateForm(QWidget *parent) : QDialog(parent)
{
    QSettings settings;
    //Setup Layouts
    QVBoxLayout *mainlayout = new QVBoxLayout;
    middlelayout = new QFormLayout;
    QHBoxLayout *dialogLayout = new QHBoxLayout;

    //Define dialog button box
    QDialogButtonBox* dialogBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(dialogBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(dialogBox, SIGNAL(accepted()), this, SLOT(dialogBox_Accepted()));
    connect(dialogBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(dialogBox, SIGNAL(rejected()), this, SLOT(dialogBox_Rejected()));

    this->setWindowTitle(tr("Quick Create"));

    //Configure Form
    QLabel *title = new QLabel;
    title->setText("Quick Create");
    title->setFont(QFont("Lato",12,700));
    title->setContentsMargins(0,0,0,10);

    QLabel *descriptor = new QLabel;
    descriptor->setText("Enter Development URL (example.test)");
    descriptor->setMinimumWidth(300);
    descriptor->adjustSize();
    descriptor->setFont(QFont("Lato", 10, 700) );
    descriptor->setContentsMargins(0,0,0,10);

    url = new QLineEdit(this);

    url->setMaximumWidth(125);
    url->setContentsMargins(0,0,0,10);

    middlelayout->setAlignment(Qt::AlignLeft);

    //Add Widgets to Layouts
    middlelayout->addWidget(title);
    middlelayout->addWidget(descriptor);
    middlelayout->addRow(tr("&URL:"), this->url);
    dialogLayout->addWidget(dialogBox);

    //Add Sublayouts to Main Layout
    mainlayout->addLayout(middlelayout);
    mainlayout->addLayout(dialogLayout);

    this->setMinimumWidth(descriptor->width());

    this->setLayout(mainlayout);
    this->adjustSize();
}

quickCreateForm::~quickCreateForm()
{
    delete middlelayout;
}

void quickCreateForm::dialogBox_Accepted()
{
    QString qUrl = url->displayText();
    if (!url->displayText().trimmed().isEmpty()) {
        createSite* cS = new createSite;
        cS->quickCreate(qUrl);
        delete cS;
        provisionSite* pS = new provisionSite;
        QString key = urlToKey(qUrl);
        pS->provision(key);
        delete pS;
    }
    else {
        QMessageBox emptyRequired;
        emptyRequired.setText("URL cannot be blank.");
        emptyRequired.exec();
    }
}

void quickCreateForm::dialogBox_Rejected()
{

}
