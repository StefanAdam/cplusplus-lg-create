#include "multicreateform.h"

multicreateForm::multicreateForm(QWidget *parent) : QDialog(parent)
{
    //Setup Layouts
    QVBoxLayout *mainlayout = new QVBoxLayout;
    middlelayout = new QFormLayout;
    QHBoxLayout *buttonlayout = new QHBoxLayout;
    QPushButton *addbutton = new QPushButton;
    QPushButton *deletebutton = new QPushButton;
    QHBoxLayout* dialogLayout = new QHBoxLayout;

    //Define dialog button box
    QDialogButtonBox* dialogBox = new QDialogButtonBox(QDialogButtonBox::Ok | QDialogButtonBox::Cancel);
    connect(dialogBox, SIGNAL(accepted()), this, SLOT(accept()));
    connect(dialogBox, SIGNAL(accepted()), this, SLOT(dialogBox_Accepted()));
    connect(dialogBox, SIGNAL(rejected()), this, SLOT(reject()));
    connect(dialogBox, SIGNAL(rejected()), this, SLOT(dialogBox_Rejected()));

    //Configure Buttons
    addbutton->setText("Add URL");
    deletebutton->setText("Remove URL");

    //Add Widgets to Layouts
    buttonlayout->addWidget(addbutton);
    buttonlayout->addWidget(deletebutton);
    dialogLayout->addWidget(dialogBox);
    buttonlayout->setAlignment(Qt::AlignTop);

    //Add Sublayouts to Main Layout
    mainlayout->addLayout(buttonlayout);
    mainlayout->addLayout(middlelayout);
    mainlayout->addLayout(dialogLayout);




    this->setLayout(mainlayout);

    connect(addbutton, SIGNAL(clicked()), SLOT(addButton_Clicked()));
    connect(deletebutton, SIGNAL(clicked()), SLOT(deleteButton_Clicked()));


}

multicreateForm::~multicreateForm()
{
    delete middlelayout;
}

void multicreateForm::dialogBox_Accepted() {
    qDebug() << "Accepted";



    QStringList qUrls;




    for (int x = 0; x < qlist.count(); ++x) {
        QLineEdit* listItem;
        listItem = qlist.at(x);
        qUrls << listItem->text();
    }

    for (int x = 0; x < qUrls.count(); ++x) {
        qDebug() << qUrls[x];
    }

    createSite* cS = new createSite;
    cS->multisiteCreate(qUrls);
    delete cS;

    QString qUrl = qUrls[0];

    provisionSite* pS = new provisionSite;
    QString key = urlToKey(qUrl);
    pS->provision(key);
    delete pS;
}

void multicreateForm::dialogBox_Rejected() {
    qDebug() << "Declined";
}

void multicreateForm::addButton_Clicked() {
    qDebug() << "Add";
    QLineEdit *lineedit = new QLineEdit;
    qlist.push_back(lineedit);
    middlelayout->addRow(tr("&URL:"), lineedit);
    lineedit->setObjectName("url"+QString::number(qlist.count()));


}

void multicreateForm::deleteButton_Clicked() {
    qDebug() << "Delete";
    if (middlelayout->rowCount() != 0) {
        middlelayout->removeRow(middlelayout->rowCount()-1);
    }
    this->adjustSize();
}
