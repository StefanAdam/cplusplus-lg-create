#include "fullcreateform.h"
#include "ui_fullcreateform.h"


fullCreateForm::fullCreateForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::fullCreateForm)
{
    ui->setupUi(this);
}

fullCreateForm::~fullCreateForm()
{
    delete ui;
}

void fullCreateForm::on_buttonBox_accepted()
{
    QString url = ui->urlInput->displayText();
    QString user = ui->userInput->displayText();
    QString pass = ui->passInput->displayText();
    QString email = ui->emailInput->displayText();
    QString title = ui->titleInput->displayText();
    QString dbprefix = ui->dbPrefixInput->displayText();
    QString multisite = ui->multisiteComboBox->currentText().toLower();
    QString xipio = ui->xipIOComboBox->currentText().toLower();
    QString delplugins = ui->deleteDefaultPluginsComboBox->currentText().toLower();
    QString delthemes = ui->deleteDefaultThemesComboBox->currentText().toLower();
    QString wp = ui->installWordPressComboBox->currentText().toLower();

    createSite* cS = new createSite;
    cS->fullCreateLegacy(url, user, pass, email, title, dbprefix, multisite, xipio, delplugins, delthemes, wp);
    delete cS;
    provisionSite* pS = new provisionSite;
    QString key = urlToKey(url);
    pS->provision(key);
    delete pS;
}

