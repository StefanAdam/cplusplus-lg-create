#include "network.h"
#include <common.h>
#include <settingsform.h>

network::network()
{

}

void network::getLatestVersion() {
        connect(&manager, SIGNAL(finished(QNetworkReply*)), this, SLOT(process_version(QNetworkReply*)));
        QUrl url;
        url.setUrl("http://img.longevitystaging.com/lgcreate/latest_version.txt");
        QNetworkRequest request(url);
        QNetworkReply *reply = manager.get(request);
        latestVersion = reply;
}

void network::process_version(QNetworkReply *reply) {
    QUrl url = reply->url();
    if (reply->error()) {
        qDebug() << "NETWORK ERROR";
        reply->abort();
        delete reply;
        qFatal("NETWORK ERROR");
    }
    else {
        qDebug() << reply;
        QString data = reply->readAll();
        qDebug() << data;
        reply->close();
        delete reply;
        QSettings settings("AlteroPax Industries", "LG-Create");
        settings.setValue("latestVersion", data);
    }
}


void network::updateLatest(const QUrl &url) {
    connect(&manager, SIGNAL(finished(QNetworkReply*)), SLOT(downloadFinished(QNetworkReply*)));
    QNetworkRequest request(url);
    QNetworkReply *reply = manager.get(request);
    connect (this, SIGNAL(downloadStarted()), this, SLOT(downloadProgressBox()));
    emit downloadStarted();
    connect(reply, &QNetworkReply::downloadProgress, this, &network::downloadProgress);
    latestVersion = reply;
}

QString network::saveFileName(const QUrl &url) {
    QSettings settings("AlteroPax Industries", "LG-Create");
    QString path = url.path();
    QString basename = QFileInfo(path).fileName();

    if (basename.isEmpty()) {
        basename= "lg-create";
        basename += settings.value("latestVersion").toString();
    }

    return basename;
}

bool network::saveToDisk(const QString &filename, QIODevice *data) {
    QFile file(filename);
    if (!file.open(QIODevice::WriteOnly)) {
        qDebug() << "Could not open " << filename << "for writing";
        qDebug() << filename;
        qDebug() << file.errorString();
        qDebug() << data->readAll();
        return false;
    }
    file.write(data->readAll());
    file.close();

    return true;
}

void network::downloadFinished(QNetworkReply* reply) {
    QUrl url = reply->url();
    if (reply->error()) {
        qDebug() << "Download Failed \n";
        qDebug() << url;
        qDebug() << reply->errorString();
    }
    else {
        QString filename = saveFileName(url);
        if (saveToDisk(filename, reply)) {{
                qDebug() << "Download Succeeded";
                qDebug() << url;

            }}
    }
    reply->deleteLater();
    settingsForm* settingsPage = new settingsForm;
    connect(this, SIGNAL(downloadCompleted()), settingsPage, SLOT(updateDownloaded()));
    connect(this, SIGNAL(downloadCompleted()), this, SLOT(downloadProgressCompleted()));
    emit downloadCompleted();
    settingsPage->deleteLater();
}


void network::downloadProgressBox() {
    dlprogress = new QWidget;
    QVBoxLayout* dlLayout = new QVBoxLayout(dlprogress);
    dlprogressBar = new QProgressBar;
    dlprogressText = new QLabel;

    dlprogressText->setText("Downloaded: "+QString::number(0)+"\n"+"Total: "+QString::number(0));

    dlLayout->addWidget(dlprogressBar);
    dlLayout->addWidget(dlprogressText);
    dlprogressBar->setRange(0,100);
    dlprogressBar->setValue(0);
    dlprogress->setLayout(dlLayout);
    dlprogress->show();


}

void network::downloadProgress(qint64 ist, qint64 max) {

    qDebug() << ist;
    dlprogressBar->setRange(0,max);
    dlprogressBar->setValue(ist);
    qint64 currentMB = ist / 1024 / 1024;
    qint64 maxMB = max / 1024 / 1024;
    dlprogressText->setText("Downloaded: "+QString::number(currentMB)+" MB\n"+"Total: "+QString::number(maxMB)+" MB");
    dlprogress->update();

}

void network::downloadProgressCompleted() {
    dlprogress->close();
}
