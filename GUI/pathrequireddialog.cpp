#include "pathrequireddialog.h"
#include "ui_pathrequireddialog.h"


pathRequiredDialog::pathRequiredDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::pathRequiredDialog)
{
    ui->setupUi(this);
}

pathRequiredDialog::~pathRequiredDialog()
{
    delete ui;
}

void pathRequiredDialog::on_buttonBox_rejected()
{
    QMessageBox failure;
    failure.setText("Error: vvvPath not Specified");
    failure.setWindowTitle("Fatal Error");
    failure.exec();
    qFatal("Required Argument");
}
