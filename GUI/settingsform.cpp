#include "settingsform.h"


settingsForm::settingsForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::settingsForm)
{
    ui->setupUi(this);
    updatePathLabel();
    updateDebugCombo();
}

settingsForm::~settingsForm()
{
    delete ui;

}

void settingsForm::on_changePathButton_clicked()
{
    //QString fileName;
    //fileName = QFileDialog::getOpenFileName(this, tr("Select VVV Custom File"), "", tr("YML Files (*.yml)"));
    //qDebug() << fileName;

    QString fileName = getPath(this);
    qDebug() << "on_changePathButton_clicked() returns: " << fileName;

    setPath(fileName);
    updatePathLabel();

}

void settingsForm::updatePathLabel() {
    QSettings settings("AlteroPax Industries", "LG-Create");
    QString vvvPath = settings.value("vvvPath").toString();

    ui->vvvPathLabel->setText(vvvPath);
}

void settingsForm::updateDebugCombo() {
    QSettings settings("AlteroPax Industries", "LG-Create");

    if(!settings.contains("debug")) {
        settings.setValue("debug", 0);
    }
    int debug = settings.value("debug").toInt();
    qDebug() << "Debug Level in Settings: " << debug;
    ui->debugLevel->setCurrentIndex(debug);
}

void settingsForm::on_debugLevel_currentIndexChanged(int index)
{
    QSettings settings("AlteroPax Industries", "LG-Create");
    settings.setValue("debug", index);
    qDebug() << "Index: " << index;
    qDebug() << "Debug Setting after index: " << settings.value("debug");
}

void settingsForm::on_updateButton_clicked()
{
    QSettings settings("AlteroPax Industries", "LG-Create");
    network* networker = new network;
    networker->getLatestVersion();
    QVersionNumber latestVersion = QVersionNumber::fromString(settings.value("latestVersion").toString());

   qDebug() << QVersionNumber::compare(latestVersion, version);
   qDebug() << latestVersion;
   qDebug() << version;

    if(QVersionNumber::compare(latestVersion, version) <= 0) {
       QMessageBox* updateMessage = new QMessageBox(this);
       updateMessage->setButtonText(1, "Ok");
       updateMessage->setText("Current Version: "+ version.toString() + "\n" +"Latest Version: "+settings.value("latestVersion").toString());
       updateMessage->exec();
    }

    //if(settings.value("latestVersion") > version) {
    if(QVersionNumber::compare(latestVersion, version) >= 0  ) {
        QDialog* updateMessage = new QDialog;
        QDialogButtonBox* dialogBox = new QDialogButtonBox;
        QPushButton* updateButton = new QPushButton(tr("&Update Now"));
        QPushButton* cancelButton = new QPushButton(tr("&Cancel"));
        QVBoxLayout* mainlayout = new QVBoxLayout(updateMessage);

        QLabel* label = new QLabel(updateMessage);

        label->setText("Current Version: "+ version.toString() + "\n" +"Latest Version: "+settings.value("latestVersion").toString());

        mainlayout->addWidget(label);
        mainlayout->addWidget(dialogBox);

        updateButton->setDefault(true);

        dialogBox->addButton(updateButton, QDialogButtonBox::ButtonRole::AcceptRole);
        dialogBox->addButton(cancelButton, QDialogButtonBox::ButtonRole::RejectRole);

        connect(dialogBox, SIGNAL(accepted()), this, SLOT(update()));
        connect(dialogBox, SIGNAL(accepted()), updateMessage, SLOT(accept()));
        connect(dialogBox, SIGNAL(rejected()), updateMessage, SLOT(reject()));
        updateMessage->exec();


    }
}

void settingsForm::update() {
    QSettings settings("AlteroPax Industries", "LG-Create");
    qDebug() << "Clicked Update";
    QString filename = "http://img.longevitystaging.com/lgcreate/lgcreate-" + settings.value("latestVersion").toString() + ".zip";
    QUrl url(filename);
    network* downloader = new network;
    downloader->updateLatest(url);

}

void settingsForm::updateDownloaded() {
    qDebug() << "Downloaded Update";
}
