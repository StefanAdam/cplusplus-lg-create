#include "common.h"



void checkSettings(QWidget *parent) {
    QSettings settings("AlteroPax Industries", "LG-Create");

    if(!settings.contains("vvvPath") || settings.value("vvvPath") == "" ){
        qDebug() << "VVV Path is unset";
        QString vvvPath = getPath(parent);
        setPath(vvvPath);
    }

    if (!settings.contains("version") || settings.value("version") == "") {
        settings.setValue("version", version.toString());
        //getLatestVersion();
    }
}


void setPath(QString vvvPath){
    QSettings settings("AlteroPax Industries", "LG-Create");
    settings.setValue("vvvPath", vvvPath);
}


QString getPath(QWidget *parent) {
    while(true) {
        QString fileName = QFileDialog::getOpenFileName(parent, QObject::tr("Select VVV Custom File"), "", QObject::tr("YML Files (*.yml)"));
        qDebug() << "getPath returns: " << fileName;
        if(!fileName.isEmpty()&& !fileName.isNull()) {
            return fileName;
        }
       pathRequiredDialog* pathRequired = new pathRequiredDialog;
       pathRequired->exec();
    }
}

QString urlToKey(QString url) {
    QStringList keyList = url.split('.');
    QString key = keyList[0];
    key.replace(QString("-"), QString(""));

    return key;
}

