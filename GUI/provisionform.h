#ifndef PROVISIONFORM_H
#define PROVISIONFORM_H

#include <QDialog>
#include "common.h"
#include "../base/provisionsite.h"

namespace Ui {
class provisionForm;
}

class provisionForm : public QDialog
{
    Q_OBJECT

public:
    explicit provisionForm(QWidget *parent = nullptr);
    ~provisionForm();

private slots:
    void on_buttonBox_accepted();

private:
    Ui::provisionForm *ui;
};

#endif // PROVISIONFORM_H
