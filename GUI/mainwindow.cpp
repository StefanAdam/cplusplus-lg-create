#include "mainwindow.h"
#include "ui_mainwindow.h"



MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    QCoreApplication::setOrganizationName("AlteroPax Industries");
    QCoreApplication::setOrganizationDomain("alteropax.tk");
    QCoreApplication::setApplicationName("LG-Create");
    QFontDatabase database;
    QList<QFont> fonts;
    foreach (const QString &family, database.families()) {
      fonts << family;
    }
    //qDebug() << fonts;
    if (fonts.contains(QFont("Lato"))) {

        QSettings settings;
        settings.setValue("lato", true);
        //QApplication::setFont(QFont("Lato"));
    }



    ui->setupUi(this);
    checkSettings(this);



}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionSettings_triggered()
{
    settingsForm* settingsPage = new settingsForm;
    settingsPage->exec();
}



void MainWindow::on_actionExit_triggered()
{
    this->close();
}



void MainWindow::on_quickCreateBtn_clicked()
{
    quickCreateForm* quickCreatePage = new quickCreateForm;
    quickCreatePage->exec();
}

void MainWindow::on_fullCreateBtn_clicked()
{
    fullCreateForm* fullCreatePage = new fullCreateForm;
    fullCreatePage->exec();
}

void MainWindow::on_advancedCreateBtn_clicked()
{
    ui->advancedCreateBtn->setText("Not Implemented");
}

void MainWindow::on_provisionBtn_clicked()
{
   provisionForm* provisionPage = new provisionForm;
   provisionPage->exec();
}


void MainWindow::on_multiCreateBtn_clicked()
{
    multicreateForm* multicreatePage = new multicreateForm;
    multicreatePage->exec();
}
