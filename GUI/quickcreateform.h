#ifndef QUICKCREATEFORM_H
#define QUICKCREATEFORM_H

#include <QDialog>
#include <QString>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QList>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QLabel>
#include <QDebug>

#include "common.h"
#include "../base/createsite.h"
#include "../base/provisionsite.h"

class quickCreateForm : public QDialog
{
    Q_OBJECT

public:
    explicit quickCreateForm(QWidget *parent = nullptr);
    ~quickCreateForm();

private slots:
    void dialogBox_Accepted();
    void dialogBox_Rejected();

private:
    QFormLayout *middlelayout;
    QLineEdit *url;
};

#endif // QUICKCREATEFORM_H
