#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFontDatabase>
#include <QList>
#include "settingsform.h"
#include "common.h"
#include "quickcreateform.h"
#include "provisionform.h"
#include "fullcreateform.h"
#include "multicreateform.h"
#include <QDebug>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_actionSettings_triggered();

    void on_actionExit_triggered();

    void on_quickCreateBtn_clicked();

    void on_fullCreateBtn_clicked();

    void on_advancedCreateBtn_clicked();

    void on_provisionBtn_clicked();



    void on_multiCreateBtn_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
