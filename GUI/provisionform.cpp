#include "provisionform.h"
#include "ui_provisionform.h"

provisionForm::provisionForm(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::provisionForm)
{
    ui->setupUi(this);
}

provisionForm::~provisionForm()
{
    delete ui;
}

void provisionForm::on_buttonBox_accepted()
{
    QString qUrl = ui->urlInput->displayText();
    provisionSite* pS = new provisionSite;
    QString key = urlToKey(qUrl);
    pS->provision(key);
    delete pS;
}
