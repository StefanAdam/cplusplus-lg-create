#ifndef MULTICREATEFORM_H
#define MULTICREATEFORM_H

#include <QWidget>
#include <QDialog>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QFormLayout>
#include <QList>
#include <QPushButton>
#include <QDialogButtonBox>
#include <QLineEdit>
#include <QLabel>
#include <QDebug>
#include <vector>
#include "../base/createsite.h"
#include "../base/provisionsite.h"
#include "../base/helpers.h"


class multicreateForm : public QDialog
{
    Q_OBJECT
public:
    explicit multicreateForm(QWidget *parent = nullptr);
    ~multicreateForm();

signals:

public slots:

private slots:
    void dialogBox_Accepted();
    void dialogBox_Rejected();
    void addButton_Clicked();
    void deleteButton_Clicked();

private:
    QFormLayout *middlelayout;
    QList<QLineEdit*> qlist;
    std::vector<QString> users;
};

#endif // MULTICREATEFORM_H
