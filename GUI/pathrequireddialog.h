#ifndef PATHREQUIREDDIALOG_H
#define PATHREQUIREDDIALOG_H

#include <QDialog>
#include <QMessageBox>

namespace Ui {
class pathRequiredDialog;
}

class pathRequiredDialog : public QDialog
{
    Q_OBJECT

public:
    explicit pathRequiredDialog(QWidget *parent = nullptr);
    ~pathRequiredDialog();

private slots:
    void on_buttonBox_rejected();

private:
    Ui::pathRequiredDialog *ui;
};

#endif // PATHREQUIREDDIALOG_H
