#ifndef SETTINGSFORM_H
#define SETTINGSFORM_H

#include <QDialog>
#include "ui_settingsform.h"
#include <QSettings>
#include <QFileDialog>
#include <QDebug>
#include <QMessageBox>
#include <QDialogButtonBox>
#include <QVBoxLayout>
#include <QLabel>
#include "common.h"
#include <network.h>

namespace Ui {
class settingsForm;
}

class settingsForm : public QDialog
{
    Q_OBJECT

public:
    explicit settingsForm(QWidget *parent = nullptr);
    ~settingsForm();

private slots:
    void on_changePathButton_clicked();

    void on_debugLevel_currentIndexChanged(int index);

    void on_updateButton_clicked();

    void update();

    void updateDownloaded();

private:
    Ui::settingsForm *ui;
    void updatePathLabel();
    void updateDebugCombo();
};

#endif // SETTINGSFORM_H
