#include <QtTest>
// add necessary includes here
#include "../base/helpers.h"
#include "../base/createsite.h"
#include "../base/helpers.h"

class TestCase : public QObject
{
    Q_OBJECT

public:
    TestCase();
    ~TestCase();

private slots:
 // void test_ClearPath();
 // void test_writeFile();
    void test_settings();

};

TestCase::TestCase()
{

}

TestCase::~TestCase()
{

}

int cleanupTestCase(){
    QSettings testsettings("AlteroPax Industries", "TEST-LG-Create");
    testsettings.clear();
    return 0;
}

//void TestCase::test_ClearPath()
//{
//    QSettings* testsettings = new QSettings("AlteroPax Industries", "TEST-LG-Create");
//    testsettings->setValue("vvvPath", "TEST VALUE");
//    clearPath(testsettings);
//    bool removed = false;
//    if (!testsettings->contains("vvvPath")) {
//        removed = true;
//    }
//    QCOMPARE(removed, true);
//}

void TestCase::test_settings()
{
    QSettings* testsettings = new QSettings("AlteroPax Industries", "TEST-LG-Create");
    testsettings->setValue("vvvPath", "TEST");
    QVariant path = testsettings->value("vvvPath");
    QCOMPARE(path, "TEST");
}

/*void TestCase::test_writeFile()
{
    QSettings* testsettings = new QSettings("AlteroPax Industries", "TEST-LG-Create");
    testsettings->setValue("vvvPath", "C:/Users/stefa/Desktop/test/testfile.txt");
    createSite* cs = new createSite;
    QString url = "test.test";
    cs->prepareInput(url);

    QString vvvPath = testsettings->value("vvvPath").toString();
    QFile vvvFile(vvvPath);

    if (vvvFile.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        // Stream text to file
        QTextStream vvvStream(&vvvFile);

        for (int x = 0; x < cs->preparedInputs.count(); x++) {
            vvvStream << cs->preparedInputs[x];

            vvvFile.close();
        }
    }
    else {
        throw "File Status non-writable exception";
    }


}
*/
QTEST_APPLESS_MAIN(TestCase)

#include "tst_testcase.moc"
